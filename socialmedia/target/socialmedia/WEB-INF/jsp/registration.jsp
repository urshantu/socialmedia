<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE html>
<html>
	<head>
	<title>User Login</title>
	<link href='https://fonts.googleapis.com/css?family=Rouge+Script' rel='stylesheet' type='text/css'>
    <jsp:include page="headtags.jsp"></jsp:include>
	<style type="text/css" media="screen">
		body{
			background-color: #ECEBEB;
			}
		.emltdp-login-form-help .col-xs-12 a{
			font-size: 10px;
			color: gray;
			font-family: 'droid_sansregular';
		}
			
        span.icon {
    		  background: url('https://google-developers.appspot.com/+/images/branding/btn_red_32.png') transparent 5px 50% no-repeat;
      		  display: inline-block;
      		  vertical-align: middle;
      		  width: 41px;
              height: 35px;
	    }
	    
	    .login-form-div{
	    	width:40%;
	    	float:right;
	    	margin-right:28%;
	    }
   
  </style>
		</head>
		<body>
			<div class="container emltdp-login-parent">
				<div class="row">
					<div class="col-xs-8 emltdp-login-form login-form-div">
						<div class="row">
							<div class="col-xs-12">
								<h1><spring:message code="login.consumerLogin" text="Registration" /></h1>
							</div>
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-12 emltdp-login-form-fields pull-left">
										<div class="row">
											<form action="login" id="user-login" method="POST">
												<div class="col-xs-6">
													<input type="text" name="first_name" value="" placeholder="First Name">
												</div>
												<div class="col-xs-6">
													<input type="text" name="last_name" value="" placeholder="Last Name">
												</div>
												<div class="col-xs-12">
													<input type="text" name="user_name" value="" placeholder="User Name">
												</div>
												<div class="col-xs-12">
													<input type="password" name="password" id="password" value="" placeholder="Password">
												</div>
												
												<div class="col-xs-12">
													<input type="submit" name="" value="REGISTER">
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</body>
	</html>