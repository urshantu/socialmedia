/*
 * Copyright (C) Emami Limited - All Rights Reserved Unauthorized copying of
 * this file, via any medium is strictly prohibited Proprietary and confidential
 * Written for Emami Limited, 2015
 */
package com.socialmedia.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.socialmedia.model.user.UserProfile;
import com.socialmedia.service.UserService;
import com.socialmedia.util.SessionManager;

@Controller
public class LoginRegistrationController {

    @Autowired
    UserService                userService;

    public static final Logger logger = LoggerFactory.getLogger(LoginRegistrationController.class);

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Inside function login()");
        ModelAndView model = new ModelAndView();
        model.setViewName("/login");
        return model;
    }

    
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Inside function logout()");
        
        UserProfile up = SessionManager.getLoggedinUser(request);
        if(up==null){
            return new ModelAndView("redirect:/login");
        }
        SessionManager.saveLoggedinUser(request, null);
        ModelAndView model = new ModelAndView();
        model.addObject("msg","Logout Successfully");
        model.setViewName("/login");
        return model;
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Inside function loginProcess()");
        ModelAndView model = new ModelAndView();
        String userName = request.getParameter("user_name");
        String password = request.getParameter("password");
        UserProfile up = userService.getUserByUserNameAndPassword(userName, password);
        if (up != null) {
            SessionManager.saveLoggedinUser(request, null);
            SessionManager.saveLoggedinUser(request, up);
            return new ModelAndView("redirect:/home");
        }
        model.addObject("msg", "Invalid username or password");
        model.setViewName("/login");
        return model;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Inside function registration()");
        ModelAndView model = new ModelAndView();
        model.setViewName("/registration");
        return model;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView saveRegistration(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("saving registration details...");
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String userName = request.getParameter("user_name");
        String password = request.getParameter("password");
        String msg = "";
        if (!userService.isExistingUserName(userName)) {
            if (userService.saveRegistrationDetails(firstName, lastName, userName, password)) {
                msg = "Registration Successfull! Please Login using your username and password";
            }
        } else {
            msg = "User name already exists";
        }
        ModelAndView model = new ModelAndView();
        model.addObject("msg", msg);
        model.setViewName("/registration");
        return model;
    }

    @RequestMapping(value = "/verify-user-name", method = RequestMethod.POST)
    @ResponseBody
    public String userNameVerification(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("saving registration details...");
        String userName = request.getParameter("user_name");
        String status = "true";
        if (userService.isExistingUserName(userName)) {
            status = "false";
        }
        return status;
    }

}