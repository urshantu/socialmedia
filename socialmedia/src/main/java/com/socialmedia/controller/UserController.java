package com.socialmedia.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.socialmedia.model.user.UserPost;
import com.socialmedia.model.user.UserProfile;
import com.socialmedia.service.UserService;
import com.socialmedia.util.CommonUtils;
import com.socialmedia.util.SessionManager;

@Controller
public class UserController {

    @Autowired
    UserService                userService;

    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response) {
        UserProfile up = SessionManager.getLoggedinUser(request);
        ModelAndView model = new ModelAndView();
        if (up == null) {
            return new ModelAndView("redirect:/login");
        }
        int uId = up.getuId();
        List<UserPost> statusList = userService.getAllStatusList(uId);
        String userFullName = up.getFirstName() + " " + up.getLastName();
        model.addObject("allStatusList", statusList);
        model.addObject("userFullName", userFullName);
        model.addObject("isHomeUser", "yes");
        model.setViewName("newsfeed");
        return model;
    }

    @RequestMapping(value = "/post-add-process", method = RequestMethod.POST)
    public ModelAndView postStatus(HttpServletRequest request, HttpServletResponse response) {
        UserProfile up = SessionManager.getLoggedinUser(request);
        if (up == null) {
            return new ModelAndView("redirect:/login");
        }
        String status = request.getParameter("status");
        userService.saveStatusPost(status, up);
        return new ModelAndView("redirect:/home");
    }

    @RequestMapping(value = "/user-newsfeed", method = RequestMethod.GET)
    public ModelAndView searchUser(HttpServletRequest request, HttpServletResponse response) {
        String userName = request.getParameter("user_name");
        UserProfile up = SessionManager.getLoggedinUser(request);
        if (up == null) {
            return new ModelAndView("/login");
        }
        int loggedInUserId = up.getuId();
        String userFullName = up.getFirstName() + " " + up.getLastName();
        up = userService.getUserByUserName(userName);
        if (up == null) {
            return new ModelAndView("/user_not_exist");
        }
        int uId = up.getuId();

        String isHomeUser = uId == loggedInUserId ? "yes" : "no";

        List<UserPost> statusList = userService.getAllStatusList(uId);
        ModelAndView model = new ModelAndView();
        model.addObject("allStatusList", statusList);
        model.addObject("userFullName", userFullName);
        model.addObject("isHomeUser", isHomeUser);
        model.setViewName("newsfeed");
        return model;
    }

    @RequestMapping(value = "/save-comment", method = RequestMethod.POST)
    @ResponseBody
    public String saveComment(HttpServletRequest request, HttpServletResponse response) {
        String userName = request.getParameter("user_name");
        UserProfile up = SessionManager.getLoggedinUser(request);
        int loggedInUserId = up.getuId();
        int pId = CommonUtils.getIntegerFromString(request.getParameter("pId"));
        String comment = request.getParameter("comment");
        if (userService.saveComment(loggedInUserId, pId, comment)) {
            return "true";
        }
        return "false";
    }

    @RequestMapping(value = "/post-like", method = RequestMethod.POST)
    @ResponseBody
    public String postLike(HttpServletRequest request, HttpServletResponse response) {
        UserProfile up = SessionManager.getLoggedinUser(request);
        int loggedInUserId = up.getuId();
        int pId = CommonUtils.getIntegerFromString(request.getParameter("pId"));
        return userService.savePostLike(loggedInUserId, pId);

    }

}
