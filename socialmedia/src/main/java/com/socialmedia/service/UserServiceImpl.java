package com.socialmedia.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialmedia.cache.UserCache;
import com.socialmedia.dao.UserDao;
import com.socialmedia.model.user.Comments;
import com.socialmedia.model.user.UserPost;
import com.socialmedia.model.user.UserProfile;
import com.socialmedia.util.CommonUtils;
import com.socialmedia.util.PasswordManager;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    public boolean saveRegistrationDetails(String firstName, String lastName, String userName, String password) {

        UserProfile userProfile = new UserProfile();

        userProfile.setFirstName(firstName);
        userProfile.setLastName(lastName);
        userProfile.setUserName(userName);
        String encryptedPassword = PasswordManager.getInstance().encrypt(password);
        userProfile.setPassword(encryptedPassword);
        if (userDao.saveRegistrationDetails(userProfile)) {
            String name = firstName + " " + lastName;
            UserCache.addUpdateMap(name, userProfile.getuId());
            return true;
        }
        return false;
    }

    public boolean isExistingUserName(String userName) {
        return userDao.isExistingUserName(userName);
    }

    public UserProfile getUserByUserNameAndPassword(String userName, String password) {
        password = PasswordManager.getInstance().encrypt(password);
        return userDao.getUserByUserNameAndPassword(userName, password);
    }

    public boolean saveStatusPost(String status, UserProfile ulp) {
        UserPost userPost = new UserPost();
        if (CommonUtils.isNotNullAndNotEmpty(status) && ulp != null) {
            userPost.setCreatedBy(ulp.getuId());
            userPost.setCreateTime(CommonUtils.getCurrentTimestamp());
            userPost.setStatus(status);
            userPost.setuId(ulp.getuId());
            return userDao.saveStatusPost(userPost);
        }
        return false;
    }

    public List<UserPost> getAllStatusList(int uId) {
        return userDao.getAllStatusList(uId);
    }

    public UserProfile getUserByUserName(String userName) {
        return userDao.getUserByUserName(userName);
    }

    public boolean saveComment(int uId, int pId, String comment) {
        if (CommonUtils.isNotNullAndNotEmpty(comment)) {
            Comments comments = new Comments();
            comments.setCommentDescription(comment.trim());
            comments.setpId(pId);
            comments.setCreatedBy(uId);
            comments.setuId(uId);
            comments.setCreateTime(CommonUtils.getCurrentTimestamp());
            return userDao.saveComment(comments);
        }
        return false;

    }

    public String savePostLike(int uId, int pId) {
        UserPost up = userDao.getPostById(pId);
        if (up != null) {
            String ids = up.getLikeIds();
            List<String> idString = ids != null && !"".equalsIgnoreCase(ids) ? (Arrays.asList(ids.split(","))) : new ArrayList<String>();
            if (idString != null && !idString.contains("" +uId)) {
                if (CommonUtils.isNotNullAndNotEmpty(ids)) {
                    ids += "," + uId;
                } else {
                    ids = "" + uId;
                }

                int likeCount = up.getLikeCount();
                likeCount++;
                userDao.savePostLike(ids, pId, likeCount);

                likeCount = likeCount == 1 ? likeCount : (likeCount==2?2: likeCount-1);

                return "" + likeCount;
            } else {
                return ""+0;
            }
        }
        return null;
    }

}
