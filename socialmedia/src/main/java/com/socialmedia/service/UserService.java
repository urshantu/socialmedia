package com.socialmedia.service;

import java.util.List;

import com.socialmedia.model.user.UserPost;
import com.socialmedia.model.user.UserProfile;


public interface UserService {

    boolean saveRegistrationDetails(String firstName, String lastName, String userName, String password);

    boolean isExistingUserName(String userName);
    
    UserProfile getUserByUserNameAndPassword(String userName, String password);
    
    UserProfile getUserByUserName(String userName);
    
    boolean saveStatusPost(String status, UserProfile up);
    
    List<UserPost>getAllStatusList(int uId);
    
    boolean saveComment(int uId, int pId, String comment);
    
    String savePostLike(int uId, int pId);
    
}
