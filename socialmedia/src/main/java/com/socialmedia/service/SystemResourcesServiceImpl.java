package com.socialmedia.service;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialmedia.cache.UserCache;
import com.socialmedia.dao.UserDao;
import com.socialmedia.model.user.UserProfile;

@Service("systemResourcesService")
public class SystemResourcesServiceImpl implements SystemResourcesService {

    @Autowired 
    UserDao userDao;

    public void initUserMap(){
        System.out.println("------Cache loaded from db ------");
        List<UserProfile>userList = userDao.getAllUsers();
        if(userList!=null && userList.size()>0){
            for(UserProfile up : userList){
                if(up!=null){
                    String name = up.getFirstName()+" "+up.getLastName();
                    int id = up.getuId();
                    UserCache.addUpdateMap(name, id);
                }
            }
        }
    }
    
}
