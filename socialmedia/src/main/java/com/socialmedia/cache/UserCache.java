package com.socialmedia.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserCache {

    public static Map<Integer, String> userIdToNameMap = new ConcurrentHashMap<Integer, String>();

    public static void addUpdateMap(String name, Integer id) {
        userIdToNameMap.put(id, name);
    }

    public static String getUserNameById(int id) {
        return userIdToNameMap.get(id);
    }

}
