package com.socialmedia.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

public final class PasswordManager {
    private static PasswordManager instance;

    private PasswordManager() {
    }

    public String encrypt(String plaintext) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("");
        }
        try {
            md.update(plaintext.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("");
        }

        byte raw[] = md.digest();
        String hash = new String(Base64.encodeBase64(raw));
        return hash;
    }

    public static PasswordManager getInstance() {
        if (instance == null) {
            instance = new PasswordManager();
        }
        return instance;
    }

    public static void main(String args[]) {
        System.out.println(PasswordManager.getInstance().encrypt("socialMedia123"));
    }

}