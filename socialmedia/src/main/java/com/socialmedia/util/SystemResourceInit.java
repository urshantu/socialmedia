package com.socialmedia.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;

public class SystemResourceInit implements BeanFactoryAware {
    public BeanFactory         context;
    public static final Logger logger                = LoggerFactory.getLogger(SystemResourceInit.class);
    public static boolean      isCronJobsInitialized = false;

    public void init() {
        logger.info("System user name : " + System.getProperty("user.name"));
        logger.info("System cache initializing");
        if (!ReloadableResourcesUtil.isSystemInitialized()) {
            ReloadableResourcesUtil.initializeAllResources();
        }
        logger.info("System cache initialized");
        
        ReloadableResourcesUtil.getInstance().initUserMap();
        
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        ReloadableResourcesUtil.getInstance().context = beanFactory;
    }

}
