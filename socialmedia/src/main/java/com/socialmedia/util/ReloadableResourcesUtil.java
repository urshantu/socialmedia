
package com.socialmedia.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

import com.socialmedia.service.SystemResourcesService;


@Component
public class ReloadableResourcesUtil implements BeanFactoryAware {

    public BeanFactory                     context             = null;
    private static ReloadableResourcesUtil rru                 = new ReloadableResourcesUtil();
    private static boolean                 resourceInitialized = false;
    private static Logger                  logger              = LoggerFactory.getLogger(ReloadableResourcesUtil.class);

    public static ReloadableResourcesUtil getInstance() {
        if (rru == null) {
            rru = new ReloadableResourcesUtil();
        }
        return rru;
    }

    public static boolean isSystemInitialized() {
        return resourceInitialized;
    }

    public synchronized static void initializeAllResources() {
        IReloadableResources[] allResources = ReloadableResources.values();
        for (IReloadableResources resource : allResources) {
            try {
                resource.init();
                logger.debug("Initialized Resource : " + resource.toString());
            } catch (Exception e) {
                logger.error("Initialized Resource : " + resource.toString() + " Fail", e);
            }
        }

        // tomcat node triggering
//        ReloadableResourcesUtil.getInstance().initializeDatabaseCacheMap();
  //      resourceInitialized = true;
    }

    @Override
    public void setBeanFactory(final BeanFactory beanFactory) {
        this.context = beanFactory;
    }
    
    public void initUserMap(){
        SystemResourcesService service = (SystemResourcesService) context.getBean("systemResourcesService");
        service.initUserMap();
    }
}
