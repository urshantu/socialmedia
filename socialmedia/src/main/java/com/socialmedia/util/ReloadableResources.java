


package com.socialmedia.util;

public enum ReloadableResources implements IReloadableResources {

    USERP_ROFILE("user profile") {
        @Override
        public void init() {
            ReloadableResourcesUtil.getInstance().initUserMap();;
        }
    };
    private String displayName;

    private ReloadableResources(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
