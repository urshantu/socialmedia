package com.socialmedia.util;

import java.sql.Timestamp;
import java.util.Calendar;

public class CommonUtils {

    public static boolean isNotNullAndNotEmpty(String str) {
        if (str != null && !"".equals(str.trim())) {
            return true;
        }
        return false;
    }

    public static Timestamp getCurrentTimestamp() {
        Calendar calendar = Calendar.getInstance();
        return new Timestamp(calendar.getTimeInMillis());
    }

    public static int getIntegerFromString(String numberStr) {
        if (numberStr == null) {
            return 0;
        }
        try {
            return Integer.parseInt(numberStr.trim());
        } catch (NumberFormatException ne) {
            return 0;
        }
    }
    
}
