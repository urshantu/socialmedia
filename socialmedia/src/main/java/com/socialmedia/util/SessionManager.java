package com.socialmedia.util;

import javax.servlet.http.HttpServletRequest;

import com.socialmedia.model.user.UserProfile;

public class SessionManager {

    public static final String LOGGED_IN_USER = "LOGGED_IN_USER";

    public static void saveLoggedinUser(HttpServletRequest request, UserProfile userProfile) {
        request.getSession().setAttribute(LOGGED_IN_USER, userProfile);
    }

    public static UserProfile getLoggedinUser(HttpServletRequest request) {
        return (UserProfile) request.getSession().getAttribute(LOGGED_IN_USER);
    }

}
