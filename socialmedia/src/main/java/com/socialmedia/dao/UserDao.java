package com.socialmedia.dao;

import java.util.List;

import com.socialmedia.model.user.Comments;
import com.socialmedia.model.user.UserPost;
import com.socialmedia.model.user.UserProfile;

public interface UserDao {

    boolean saveRegistrationDetails(UserProfile userProfile);

    boolean isExistingUserName(String userName);

    UserProfile getUserByUserNameAndPassword(String userName, String password);

    UserProfile getUserByUserName(String userName);

    boolean saveStatusPost(UserPost up);

    List<UserPost> getAllStatusList(int uId);

    boolean saveComment(Comments comments);

    List<UserProfile> getAllUsers();

    UserPost getPostById(int pId);

    public boolean savePostLike(String likeIds, int pid, int count);

}
