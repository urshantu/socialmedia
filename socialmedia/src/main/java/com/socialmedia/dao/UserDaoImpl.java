package com.socialmedia.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.socialmedia.model.user.Comments;
import com.socialmedia.model.user.UserPost;
import com.socialmedia.model.user.UserProfile;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

    private static Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
    @Autowired
    SessionFactory        sessionFactory;

    @Override
    public boolean saveRegistrationDetails(UserProfile userProfile) {
        logger.debug("saving registration details...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            session.saveOrUpdate(userProfile);
            session.close();
            return true;
        } catch (Exception e) {
            logger.debug("exception while saving registration details... ");
            return false;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return false;
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isExistingUserName(String userName) {
        logger.debug("checking for duplicate user name...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from UserProfile where user_name=:USER_NAME").setString("USER_NAME", userName);
            List<UserProfile> userList = query.list();
            session.close();
            if (userList != null && userList.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            logger.debug("exception while getting user name... ");
            return false;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return false;
            }
        }

    }

    @SuppressWarnings("unchecked")
    public UserProfile getUserByUserNameAndPassword(String userName, String password) {
        logger.debug("inside getUserByUserNameAndPassword...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from UserProfile where user_name=:USER_NAME and password=:PASSWORD").setString("USER_NAME", userName)
                    .setString("PASSWORD", password);
            List<UserProfile> userList = query.list();
            session.close();
            if (userList != null && userList.size() > 0) {
                return userList.get(0);
            }
            return null;
        } catch (Exception e) {
            logger.debug("exception while getting user name... ");
            return null;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return null;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public UserProfile getUserByUserName(String userName) {
        logger.debug("inside getUserByUserName...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from UserProfile where user_name=:USER_NAME").setString("USER_NAME", userName);
            List<UserProfile> userList = query.list();
            session.close();
            if (userList != null && userList.size() > 0) {
                return userList.get(0);
            }
            return null;
        } catch (Exception e) {
            logger.debug("exception while getting getUserByUserName... ");
            return null;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return null;
            }
        }
    }

    public boolean saveStatusPost(UserPost up) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            session.getTransaction().begin();;
            session.saveOrUpdate(up);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e) {
            logger.debug("exception while saving saveStatusPost details... ");
            return false;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return false;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public List<UserPost> getAllStatusList(int uId) {
        logger.debug("inside getAllStatusList...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from UserPost where created_by=:USER_ID").setInteger("USER_ID", uId);
            List<UserPost> statusList = query.list();
            if (statusList != null && statusList.size() > 0) {
                for (UserPost up : statusList) {
                    Hibernate.initialize(up.getComments());
                    Hibernate.initialize(up.getUserProfile());
                }
            }
            session.close();
            return statusList;
        } catch (Exception e) {
            logger.debug("exception while getting getAllStatusList... ");
            return null;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return null;
            }
        }

    }

    public boolean saveComment(Comments comments) {
        Session session = null;
        try {
            session = sessionFactory.openSession();
            session.saveOrUpdate(comments);
            session.close();
            return true;
        } catch (Exception e) {
            logger.debug("exception while saving saveComment details... ");
            return false;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return false;
            }
        }
    }

    public List<UserProfile> getAllUsers() {
        logger.debug("inside getAllUsers...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from UserProfile up ");
            List<UserProfile> userList = query.list();
            session.close();
            return userList;
        } catch (Exception e) {
            logger.debug("exception while getting getAllUsers... ");
            return null;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return null;
            }
        }

    }

    @SuppressWarnings("unchecked")
    public UserPost getPostById(int pId) {
        logger.debug("inside getPostById...");
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Query query = session.createQuery("from UserPost up where p_id="+pId);
            List<UserPost> postList = query.list();
           
            UserPost up = null;
            if (postList != null && postList.size() > 0) {
                up =  postList.get(0);
                Hibernate.initialize(up.getComments());
            }
            session.close();
            return up;
        } catch (Exception e) {
            logger.debug("exception while getting getPostById... ");
            return null;
        } finally {
            try {
                if (session != null && session.isOpen()) {
                    session.close();
                }
            } catch (Exception e) {
                logger.debug("exception while closing connection...");
                return null;
            }
        }
    }
    
    @SuppressWarnings("unused")
    public boolean savePostLike(String likeIds, int pid,int count){
        Session session= null;
        try{
            session = sessionFactory.openSession();
            Query query = session.createSQLQuery("update post set like_count="+count+",like_ids='"+likeIds+"' where p_id="+pid);
            int result = query.executeUpdate();
            session.close();
            return true;
        }catch(Exception e){
            return false;
        }finally{
            try{
                if(session!=null && session.isOpen()){
                    session.close();
                }
            }catch(Exception e){
                return false;
            }
        }
    }
    

}
