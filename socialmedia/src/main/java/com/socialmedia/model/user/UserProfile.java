package com.socialmedia.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_profile")
public class UserProfile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "u_id")
    private int            uId;

    @Column(name = "first_name", length = 100)
    private String         firstName;

    @Column(name = "last_name", length = 100)
    private String         lastName;

    @Column(name = "user_name", length = 100)
    private String         userName;

    @Column(name = "password")
    private String         password;

    @OneToMany(mappedBy = "userProfile")
    private List<UserPost> statusList;

    @OneToMany(mappedBy = "userProfile")
    private List<Comments> commentsList;

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserPost> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<UserPost> statusList) {
        this.statusList = statusList;
    }

}
