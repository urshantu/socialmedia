package com.socialmedia.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "social_media_app")
public class SocialMediaApp implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID   = 1L;
    @Transient
    private double            clinicPriceDisplay = 0.0;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int               id;

    @Column(name = "u_id")
    private Integer           uId;

    @Column(name = "name", length = 100)
    private String            name;

}
