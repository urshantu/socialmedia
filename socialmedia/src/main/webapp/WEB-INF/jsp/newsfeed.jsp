<!DOCTYPE html>

<%@page import="com.socialmedia.util.SessionManager"%>
<%@page import="com.socialmedia.model.user.UserProfile"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.socialmedia.cache.UserCache"%>
<%@page import="com.socialmedia.model.user.Comments"%>
<%@page import="com.socialmedia.util.CommonUtils"%>
<%@page import="com.socialmedia.model.user.UserPost"%>
<%@page import="java.util.List"%>
<html>
	<head>
		<title>My Home</title>
		<jsp:include page="headtags.jsp"></jsp:include>
		 <style type="text/css">
		 .status-post-button{
		 	margin-top: -.8%;
    		float: right;
    		margin-right: -4%;
		 }
		 
		 .comment-input{
			border-color: #ccc;
            border-style: double;
            min-height:30px !important;
    		margin-top: 7px !important;
		    border: 1px solid #b0b0b1 !important;
		    background-color: #fbfcfc !important;
		    padding: 4px 9px !important;
		    color: #9b9b9b !important;
		    font-size: 14px !important;
		    font-family: arial !important;
		    width: 104% !important;
		    margin-left: .5% !important;
            
		 }
		 
		 .comment-user-name{
		 	background-color: rgba(66, 139, 202, 0.19);
		 	text-transform: capitalize;
		    height: auto;
		    min-height: 39px;
		    padding: 11px;
		    width: 83.4%;
            margin-left: 2%;
		 }
		 
		 .comment-user-name span{
		 	font-size: initial;
    		font-weight: 700;
    		color: #31708f;
		 }
		 
		 </style>
	</head>
	
	<%
	List<UserPost>allStatusList = request.getAttribute("allStatusList")!=null ? (List<UserPost>)request.getAttribute("allStatusList"):null;
	
	String userFullName = (String)request.getAttribute("userFullName");
	
	String isHomeUser = (String)request.getAttribute("isHomeUser");
	
	UserProfile uProfile = SessionManager.getLoggedinUser(request);
	int loggedInUserId = uProfile!=null ? uProfile.getuId():0;
	
	
	%>
	
	<body>
		<div class="emltdp-rc-messages-sheader">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 emltdp-rc-messages-sheader-controls" style="">
					<div class="col-xs-5" style="text-transform: capitalize;">Welcome &nbsp;&nbsp;<%=userFullName%> </br><strong class="user_home" style="cursor:pointer;">Home</strong> &nbsp;&nbsp; <strong class="user_logout" style="cursor:pointer;">Logout</strong></div>
					<form action="user-newsfeed" method="get">
						<input type="text" class="search-user-class" value="" id="user_name" name="user_name" placeholder="search user">
						<input type="submit" value="Search">
					</form>
					</div>
				</div>
			</div>
		</div>
		<%if("yes".equalsIgnoreCase(isHomeUser)){%>	
		<div class="container compose-query-wrapper" style="width:68%">
			<form action="post-add-process" id="post-status-form" method="post">
			<div class="row"><div class="row emltdp-rc-messages-compose universal-shadow emltdp-status-post" style="min-height:60px;height:auto;">
				<div class="col-xs-9"><textarea id="post_textarea" name="status" placeholder="What's on your mind?"></textarea></div>
				<div class="col-xs-2 status-post-button"><input type="button" id="post-status-button" value="POST"></div></div>				
			</div>
			<div id="uploaded-files-input"></div>
			</form>
		</div>
		<%}%>
		
		<div class="container compose-query-wrapper" style="width:70%">
		<%if(allStatusList!=null && allStatusList.size()>0){
			for(UserPost userpost : allStatusList){
				if(userpost!=null && CommonUtils.isNotNullAndNotEmpty(userpost.getStatus())){%>
				<div class="row emltdp-rc-messages-compose universal-shadow emltdp-status-post" style="min-height:60px;height:auto;">
				  <div class="col-xs-12"><%=userpost.getStatus()%></div>
				  <div class="col-xs-12" style="padding-top:6px;font-size:11px;color:#428BCA">
				     <input type="hidden" id="post_like_id" value="<%=userpost.getpId()%>">
				     <span class="like_post" style="cursor: pointer;">Like</span>
				     <span style="margin-left:15px;cursor:pointer;">comment</span>
				  </div>
				  
				  <div class="col-xs-12" style="padding-top:6px;font-size:11px;color:#428BCA">
				     <%String likeString = "";
				     List<String>idString = userpost.getLikeIds()!=null  &&  !"".equalsIgnoreCase(userpost.getLikeIds()) ? (Arrays.asList(userpost.getLikeIds().split(","))):null;
				     int count = userpost.getLikeCount();
				     if(idString!=null && idString.contains(""+loggedInUserId)){
				    	 if(count==1){
				    		 likeString = "you like this"; 
				    	 }else{
				    		 
				    	 likeString = "you and "+(count-1)+" people like this";			    	 
				    	 }
				     }else if(count>0){
				    	 likeString = count+" people liked this"; 
				     }
						String disString = "display:none";				     
				     
				     if(count>0){
				    	 disString = "";
				     }%>
				         <span class ="like_count_string" style="cursor: pointer;<%=disString%>"><%=likeString%></span>

				  </div>
				  
				  <div class="user-comments-div">
				  <input type="hidden" id="pId" name="pId" value="<%=userpost.getpId()%>">
				  <input type="hidden" id="logged_in_user_name" value="<%=userFullName%>">
				  <%List<Comments>commentList = userpost.getComments();
				  int size = commentList!=null ? commentList.size():0;
				  %>
				  <input type="hidden" id="comment_count" value="<%=size%>">
				 <%if(commentList!=null && commentList.size()>0){boolean isFirst = true;
				  	for(Comments commnet : commentList){
				  		if(commnet!=null){ String name = UserCache.getUserNameById(commnet.getuId());%>
				  		<%if(isFirst){isFirst = false;%>
				  			<div class="comment-user-name" style="text-transform: capitalize; margin-top:65px;"><span ><%=name%></span>&nbsp;&nbsp;<%=commnet.getCommentDescription()%></div>
				  		<%}else{%>	
				  			<div class="comment-user-name" style="text-transform: capitalize;margin-top:1px;"><span ><%=name%></span>&nbsp;&nbsp;<%=commnet.getCommentDescription()%></div>
			     <%}}}}%></div>
			
				<div class="col-xs-10">
					<textarea class="comment-input" id="comment_textarea" name="comment" placeholder="write a comment" style="height:30px !important"></textarea>				  
				</div>
		     </div>
		<%}}}else{%>
		<strong style="text-align: center;">user did not post anything till now</strong>
		<%}%>
		</div>
			<form action="home" id="user_home_form" type="get"></form>
			<form action="logout" id="user_logout_form" type="get"></form>
			
			
		<script type='text/javascript' src='static/js/jquery.min.js' ></script>
		<script>
		$(function(){
			$("#post-status-button").click(function(){
				var status = $("#post_textarea").val();
				if(typeof status!="undefined" && status.trim()!=""){
					$("#post-status-form").submit();					
				}
			});
			
			$('.comment-input').keydown(function(event) {
				if (event.keyCode == 13) {
					var comment = $(this).val();
					if(typeof comment!="undefined" && comment.trim()!=""){
					$(this).val("");
					$(this).blur();
					var div = $($($(this).parent('div')).prev('div')).find('input');
					var pId = $(div).val();
					submitComment(pId,comment,this);
					}
			     }
			});
			
			function submitComment(pId,comment,currentDiv){
				var username = $("#logged_in_user_name").val();
				var query = {"pId":pId,
							 "comment":comment}
				$.ajax({
					type : "POST",
					url : "save-comment",
					data : query,
					cache:false,
					context: this,
					error : function() {
						alert('<p>An error has occurred</p>');
					},
					success : function(data) {
						if(typeof data!="undefined" && data==="true"){
							
							
							var commentDivShow = $($(currentDiv).parent('div')).prev('div');
							var commentCountDiv = $($($(currentDiv).parent('div')).prev('div')).find('input:hidden:eq(2)');
							
							console.log(commentDiv);
							
							var comentCount = $(commentCountDiv).val();
							
							
							var marginTop="1px;";
							if(typeof comentCount!="undefined" && comentCount <1){
								marginTop = "65px;";
								comentCount = parseInt(comentCount)+1;
								$(commentCountDiv).val(comentCount);
							}
							
							var commentDiv = '<div class="comment-user-name" style="text-transform: capitalize; margin-top:'+marginTop+'">'+
											 '<span>'+username+'</span>&nbsp;&nbsp;'+comment+'</div>';
							$(commentDivShow).append(commentDiv);				 
						    
						}
					}
				});				
			}
			
			$(".like_post").click(function(){
				var div = $($(this).parent('div')).find('input');
				var pId = $(div).val();
				var query = {"pId":pId,
							"post":"llike"};
				$.ajax({
					type : "POST",
					url : "post-like",
					data : query,
					cache:false,
					context: this,
					error : function() {
						alert('<p>An error has occurred</p>');
					},
					success : function(data) {
						if(data!="0"){
							var div = $($($(this).parent('div')).next('div')).find('span');
							if(data==="1"){
								console.log(div);
								$(div).text("you like this");
								$(div).show();
							}else if(data==="2"){
								$(div).text("you and 1 people like this");
							}else{
								$(div).text("you and "+data+" people like this");
							}
						}
					}
				});			
			});
			
			
			$('.user_home').click(function(){
				$("#user_home_form").submit();
			});
			
			$('.user_logout').click(function(){
				$("#user_logout_form").submit();
			});
			
		});
		</script>
	</body>
</html>