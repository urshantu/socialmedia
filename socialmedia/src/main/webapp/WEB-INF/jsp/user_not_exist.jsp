<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<title>User Not Found</title>
		<jsp:include page="headtags.jsp"></jsp:include>
		 <style type="text/css">
		 .status-post-button{
		 	margin-top: -.8%;
    		float: right;
    		margin-right: -4%;
		 }
		 </style>
</head>
<body>

<div class="emltdp-rc-messages-sheader">
			<div class="container">
				<div class="row">
				<form action="home" id="home_form_submit" method="get">
					<div class="col-xs-2" id="go_home" style="text-transform: capitalize;font-weight:bold;cursor: pointer;">Home</div>
				</form>
					<div class="col-xs-10 emltdp-rc-messages-sheader-controls" style="">
					<form action="user-newsfeed" method="get">
						<input type="text" class="search-user-class" value="" id="user_name" name="user_name" placeholder="search user">
						<input type="submit" value="Search">
					</form>
					</div>
				</div>
			</div>
</div>

<h1><strong style="float:right;margin-right:25%">No user exist for your search query</strong></h1>
<script type='text/javascript' src='static/js/jquery.min.js' ></script>
<script>
$(function(){
	$("#go_home").click(function(){
		$("#home_form_submit").submit();
	});
});
</script>

</body>
</html>