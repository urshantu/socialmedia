<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE html>
<html>
	<head>
	<title>User Registration</title>
	<link href='https://fonts.googleapis.com/css?family=Rouge+Script' rel='stylesheet' type='text/css'>
    <jsp:include page="headtags.jsp"></jsp:include>
	<style type="text/css" media="screen">
		body{
			background-color: #ECEBEB;
			}
		.emltdp-login-form-help .col-xs-12 a{
			font-size: 10px;
			color: gray;
			font-family: 'droid_sansregular';
		}
	   
	    .login-form-div{
	    	width:40%;
	    	float:right;
	    	margin-right:28%;
	    }
	    
	    .registration-success-msg{
	    	width: 92%;
    		margin: 0 auto;
    		margin-top: -11%;
    		margin-bottom: 2%;
    		text-align: center;
	    }
	    
	    .registration-error-msg{
	    	width: 92%;
    		margin: 0 auto;
    		margin-top: -11%;
    		margin-bottom: 2%;
    		text-align: center;
	    }
   
  </style>
		</head>
		<body>
			<div class="container emltdp-login-parent">
				<div class="row">
					<div class="col-xs-8 emltdp-login-form login-form-div">
						<div class="row">
							<div class="col-xs-12">
								<h1><spring:message code="login.consumerLogin" text="Registration" /></h1>
							</div>
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-12 emltdp-login-form-fields pull-left">
										<div class="row">
											<c:if test="${not empty msg}">
												<div class="msg registration-success-msg">${msg}</div>
											</c:if>
											
												<div class="msg registration-error-msg" style="display:none">user name already exist</div>
											
											<form action="registration" id="user-registration" method="POST">
												<div class="col-xs-6">
													<input type="text" name="first_name"  id="first_name" value="" placeholder="First Name">
												</div>
												<div class="col-xs-6">
													<input type="text" name="last_name" id="last_name" value="" placeholder="Last Name">
												</div>
												<div class="col-xs-12">
													<input type="text" name="user_name" id="social_user_name" value="" placeholder="User Name">
												</div>
												<div class="col-xs-12">
													<input type="password" name="password" id="password" value="" placeholder="Password">
												</div>
												
												<div class="col-xs-12">
													<input type="password" name="retype-password" id="retype-password" value="" placeholder="Confirm Password">
												</div>
												
												<div class="col-xs-6" id="register-button">
													<input type="button"  id="reg-form-submit" name="" value="REGISTER">
												</div>
											</form>
											<form action="login" methd="get">
											    <div class="col-xs-6" id="login-button">
													<input type="submit"  id="login-form-submit" name="" value="LOGIN">
											    </div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<script type='text/javascript' src='static/js/jquery.min.js' ></script>
			<!-- <script type='text/javascript' src='static/js/bootstrap.min.js' ></script> -->
			<script>
			$(function() {
				
				/*--- TO check for user name if it is already present starts---*/
				$('#social_user_name').on('focusout', function() {
					var userName = $('#social_user_name').val();
					if(typeof userName!="undefined" && userName!=""){
					    verifyUserName(userName);
					}
				});
				
				$("#reg-form-submit").click(function(){
					var firstName = $("#first_name").val();
					var lastName = $("#last_name").val();
					var userName = $("#social_user_name").val();
					var password = $("#password").val();
					var password = $("#password").val();
					var reenterPassword = $("#retype-password").val();
					if(validate(firstName,lastName,userName,password,reenterPassword)){
						$("#user-registration").submit();
					}else{
						
					}
				});
				
			});

			function validate(firstName,lastName,userName,password,reenterPassword){
				var isVallidate = false;
				if(typeof firstName!="undefined" && firstName!="" && firstName.trim()!="" && typeof lastName!="undefined" && lastName!="" && lastName.trim()!="" && typeof userName!="undefined" && userName!="" && userName.trim()!="" && typeof password!="undefined" && password!="" && password.trim()!=""){
					isVallidate = true;
				}
				if(!isVallidate){
					$(".registration-error-msg").text("All fields are required");
					$(".registration-error-msg").show();
					return false;
				}
				if(password.trim()!=reenterPassword.trim()){
					$(".registration-error-msg").text("Password and confirm password should be same");
					$(".registration-error-msg").show();
					return false;
				}
				return true;
			}
			
			function verifyUserName(userName){
				var query = {"user_name":userName}
				$.ajax({
					type : "POST",
					url : "verify-user-name",
					data : query,
					cache:false,
					context: this,
					error : function() {
						alert('<p>An error has occurred</p>');
					},
					success : function(data) {
						if(typeof data!="undefined" && data==="false"){
							$(".registration-success-msg").hide();
							$(".registration-error-msg").text("user name already exist");
							$(".registration-error-msg").show();
							$( "#reg-form-submit" ).prop( "disabled", true );
						}else if(typeof data!="undefined" && data==="true"){
							$(".registration-success-msg").hide();
							$(".registration-error-msg").hide();
							$( "#reg-form-submit" ).prop( "disabled", false);
						}
					}
				});
			}
			</script>
		</body>
	</html>