<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE html>
<html>
	<head>
	<title>User Login</title>
	<link href='https://fonts.googleapis.com/css?family=Rouge+Script' rel='stylesheet' type='text/css'>
    <jsp:include page="headtags.jsp"></jsp:include>
	<style type="text/css" media="screen">
		body{
			background-color: #ECEBEB;
			}
		.emltdp-login-form-help .col-xs-12 a{
			font-size: 10px;
			color: gray;
			font-family: 'droid_sansregular';
		}
	    
	    .login-form-div{
	    	width:40%;
	    	float:right;
	    	margin-right:28%;
	    }
	    
	     .login-error-msg{
	    	width: 92%;
    		margin: 0 auto;
    		margin-top: -11%;
    		margin-bottom: 2%;
    		text-align: center;
	    }
   
  </style>
		</head>
		<body>
			<div class="container emltdp-login-parent">
				<div class="row">
					<div class="col-xs-8 emltdp-login-form login-form-div">
						<div class="row">
							<div class="col-xs-12">
								<h1><spring:message code="login.consumerLogin" text="Consumer Login" /></h1>
							</div>
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-12 emltdp-login-form-fields pull-left">
										<div class="row">
											<c:if test="${not empty msg}">
												<div class="msg login-error-msg">${msg}</div>
											</c:if>
											<div class="msg login-error-msg" style="display:none"></div>
											<form action="login" id="user-login-form" method="POST">
												<div class="col-xs-12">
													<input type="text" name="user_name" id="user_name" value="" placeholder="Username">
												</div>
												<div class="col-xs-12">
													<input type="password" name="password" id="password" value="" placeholder="Password">
												</div>
												<div class="col-xs-6">
													<input type="button" name="" id="user-login-button" value="LOGIN">
												</div>
											</form>
											
											<form action="registration" id="user-registration" method="GET">
												<div class="col-xs-6">
													<input type="submit" name="" value="REGISTRATION">
												</div>
											</form>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script type='text/javascript' src='static/js/jquery.min.js' ></script>
			<script>
			$(function(){
				
				$("#user-login-button").click(function(){
					var userName = $("#user_name").val();
					var password = $("#password").val();
					if(validateInput(userName,password)){
						$("#user-login-form").submit();
					}
				});
				
				function validateInput(userName,password){
					if(typeof userName!="undefined" && userName.trim()!="" && typeof password!="undefined" && password.trim()!=""){
						return true;
					}
					$(".login-error-msg").text("All fields are required");
					$(".login-error-msg").show();
					return false;
				}
				
			});
			</script>
			
		</body>
	</html>