<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1000">
<link rel="stylesheet" href="static/css/bootstrap.css">
<link rel="stylesheet" href="static/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="static/css/style.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="<c:url value="/static/js/html5shiv.min.js" />"></script>
  <script src="<c:url value="/static/js/respond.min.js" />"></script>
<![endif]-->